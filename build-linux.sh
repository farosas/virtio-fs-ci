#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Build a Linux kernel vmlinuz
#
# Copyright (C) 2019 Red Hat, Inc.

set -e

if [ ! -d include/uapi/linux ]; then
	echo 'This script must be run from a Linux source tree.'
	exit 1
fi

scriptdir=$(dirname "$0")
cp "$scriptdir/configs/config-monolithic-virtio-fs" .config

make -j$(nproc) oldconfig
make -j$(nproc) bzImage
