#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Run the virtio-fs test suite
#
# Copyright (C) 2019 Red Hat, Inc.

log() {
	echo "$(date --iso-8601=seconds --universal) $@"
}

if [ $EUID != 0 ]; then
	echo 'This script must be run as root.'
	exit 1
fi

if [ ! -f rootfs.tar.xz ]; then
	echo 'A rootfs.tar.xz root file system tarball is required'
	exit 1
fi

if [ ! -f qemu.tar.xz ]; then
	echo 'A qemu.tar.xz QEMU tarball is required'
	exit 1
fi

if [ ! -f bzImage ]; then
	echo 'A bzImage Linux kernel is required'
	exit 1
fi

log "Unpacking rootfs..."
rm -rf rootfs
tar xf rootfs.tar.xz

log "Unpacking qemu..."
rm -rf qemu
tar xf qemu.tar.xz

log "Starting virtiofs..."
rm -f vhost-fs.sock
qemu/usr/local/bin/virtiofsd \
	-o vhost_user_socket=vhost-fs.sock \
	-o "source=$PWD/rootfs" \
	-o cache=always \
	-f &
virtiofsd_pid=$!

# TODO This is racy.  Wait for the listening message on virtiofsd stderr instead.
while [ ! -e vhost-fs.sock ]; do
	sleep 0.1
done

log "Starting QEMU..."
rm -f qmp.sock
rm -f serial.log
qemu/usr/local/bin/qemu-system-x86_64 \
	-kernel bzImage \
	-append 'rootfstype=virtio_fs rootflags=default_permissions,allow_other,user_id=0,group_id=0,rootmode=040000,tag=myfs,dax root=none rw console=ttyS0 systemd.unit=run-test.service' \
	-M accel=kvm \
	-cpu host \
	-m 1G,maxmem=4G \
	-object memory-backend-file,id=mem,size=1G,mem-path=/dev/shm,share=on \
	-numa node,memdev=mem \
	-chardev socket,id=char-vhost-fs,path=vhost-fs.sock \
	-device vhost-user-fs-pci,chardev=char-vhost-fs,tag=myfs \
	-chardev socket,id=char-qmp,path=qmp.sock,server,nowait \
	-mon chardev=char-qmp,mode=control \
	-chardev file,id=char-serial,path=serial.log \
	-serial chardev:char-serial \
	-vga none \
	-display none

wait $virtiofsd_pid
log "Finished test, printing log..."

cat serial.log

if grep '^PASS$' serial.log >/dev/null; then
	exit 0
else
	exit 1
fi
