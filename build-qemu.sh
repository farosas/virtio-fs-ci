#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Build a QEMU binary and produce a qemu.tar.xz tarball
#
# Copyright (C) 2019 Red Hat, Inc.

set -e

if [ ! -f vl.c ]; then
	echo 'This script must be run from a QEMU source tree.'
	exit 1
fi

rm -rf qemu
mkdir qemu

./configure --target-list=x86_64-softmmu \
	--disable-fdt \
	--disable-glusterfs \
	--disable-guest-agent \
	--disable-numa \
	--disable-rbd \
	--disable-pvrdma \
	--enable-seccomp \
	--disable-spice \
	--disable-smartcard \
	--disable-opengl \
	--disable-usb-redir \
	--disable-tcmalloc \
	--disable-libpmem \
	--enable-vhost-user \
	--disable-avx2 \
	--audio-drv-list= \
	--disable-bluez \
	--disable-brlapi \
	--disable-cap-ng \
	--disable-curl \
	--disable-curses \
	--disable-debug-tcg \
	--disable-docs \
	--disable-gtk \
	--enable-kvm \
	--disable-libiscsi \
	--disable-libnfs \
	--disable-libssh2 \
	--disable-libusb \
	--disable-bzip2 \
	--disable-linux-aio \
	--disable-lzo \
	--enable-pie \
	--disable-qom-cast-debug \
	--disable-sdl \
	--disable-snappy \
	--disable-sparse \
	--disable-strip \
	--disable-tpm \
	--disable-vde \
	--disable-vhost-scsi \
	--disable-vxhs \
	--disable-virtfs \
	--disable-vnc-jpeg \
	--disable-vte \
	--disable-vnc-png \
	--disable-vnc-sasl \
	--disable-werror \
	--disable-xen \
	--disable-xfsctl \
	--disable-gnutls \
	--disable-gcrypt \
	--disable-nettle \
	--enable-attr \
	--disable-bsd-user \
	--disable-cocoa \
	--enable-debug-info \
	--disable-guest-agent-msi \
	--disable-hax \
	--disable-jemalloc \
	--disable-linux-user \
	--disable-modules \
	--disable-netmap \
	--disable-replication \
	--enable-system \
	--disable-tools \
	--disable-user \
	--disable-vhost-net \
	--disable-vhost-vsock \
	--disable-vnc \
	--disable-mpath \
	--disable-virglrenderer \
	--disable-xen-pci-passthrough \
	--disable-tcg \
	--disable-sanitizers \
	--disable-hvf \
	--disable-whpx \
	--enable-malloc-trim \
	--disable-membarrier \
	--disable-vhost-crypto

make "DESTDIR=$PWD/qemu" -j$(nproc) install

make -j$(nproc) virtiofsd
cp virtiofsd qemu/usr/local/bin/

tar cJf qemu.tar.xz qemu/
