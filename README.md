Virtio-fs Continuous Integration Scripts
----------------------------------------
These scripts execute automated tests for virtio-fs Linux kernel, QEMU, and
virtiofsd code.  They can be launched from CI systems like GitLab's
.gitlab-ci.yml.

The following artifacts are required to run tests:
* qemu.tar.xz - a tarball containing QEMU and virtiofsd binaries
* bzImage - a monolithic Linux kernel image
* rootfs.tar.xz - a guest root file system tarball containing a systemd
  run-test.service that executes and prints PASS or FAIL on ttyS0

The official URLs for downloading the latest artifacts are:
* https://gitlab.com/virtio-fs/virtio-fs-ci/-/jobs/artifacts/master/raw/qemu/qemu.tar.xz?job=qemu
* https://gitlab.com/virtio-fs/virtio-fs-ci/-/jobs/artifacts/master/raw/linux/arch/x86/boot/bzImage?job=linux
* https://gitlab.com/virtio-fs/virtio-fs-ci/-/jobs/artifacts/master/raw/rootfs.tar.xz?job=rootfs

Run build-qemu.sh from a QEMU source tree to produce qemu.tar.xz.

Run build-linux.sh from a Linux source tree to produce bzImage.

Run build-rootfs.sh to produce rootfs.tar.xz.

The run-test.sh script launches virtiofsd on the rootfs and then launches QEMU.
Guest serial port output is saved to serial.log and test is considered a
success if PASS was printed on the serial port before the guest terminated.

For questions, please contact virtio-fs@redhat.com or #virtio-fs on
chat.freenode.net.
